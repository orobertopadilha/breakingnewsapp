import React from 'react'

import { Box, Text } from 'native-base'

const CategoryItem = ({ id, name }) => {

    return (        
        <Box padding={4} 
            borderBottomColor={'#cdcdcd'} 
            borderBottomWidth='0.5px'>
            <Text>{name}</Text>
        </Box>            
    )
}

export default CategoryItem