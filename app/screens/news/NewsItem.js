import moment from 'moment'
import { Heading, Text } from 'native-base'
import React from 'react'

import CardView from '../../components/CardView'

const NewsItem = ({ news }) => {
    return (
        <CardView>
            <Heading size="xs">
                {news.title}
            </Heading>
            <Text marginTop={2}>{news.description}</Text>
            <Text marginTop={2} fontSize="xs" italic>
                {moment(news.publishedAt).format('DD/MM/YYYY HH:mm')}
            </Text>
        </CardView>
    )
}

export default NewsItem