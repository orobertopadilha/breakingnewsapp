import { NativeBaseProvider } from "native-base";
import React from "react";
import NewsAppRouter from "./app/router/NewsAppRouter";

const App = () => {

    return (
        <NativeBaseProvider>
            <NewsAppRouter />
        </NativeBaseProvider>
    )
}


export default App